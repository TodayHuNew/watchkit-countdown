//
//  ViewController.m
//  Animation Generation
//
//  Created by Daniel Larsen on 1/28/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "AnimationGenerationController.h"

@interface AnimationGenerationController ()

@property (weak) IBOutlet NSSlider *segment1Slider;
@property (weak) IBOutlet NSSlider *segment2Slider;
@property (weak) IBOutlet NSSlider *framesPerSecondSlider;

@property (weak) IBOutlet NSTextField *segment1Label;
@property (weak) IBOutlet NSTextField *segment2Label;
@property (weak) IBOutlet NSTextField *framesPerSecondLabel;

@property (weak) IBOutlet NSImageView *previewImage;

@end

@implementation AnimationGenerationController

- (void)viewDidLoad {
	[super viewDidLoad];

	// Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
	[super setRepresentedObject:representedObject];

	// Update the view, if already loaded.
}

#pragma mark - Actions

- (IBAction)segment1Update:(id)sender {
	self.segment1Label.stringValue = [NSString stringWithFormat:@"%li", (long)self.segment1Slider.integerValue];
	[self updatePreview];
}

- (IBAction)segment2Update:(id)sender {
	self.segment2Label.stringValue = [NSString stringWithFormat:@"%1.1ld", (long)self.segment2Slider.integerValue];
	[self updatePreview];
}

- (IBAction)framesPerSecondUpdate:(id)sender {
	self.framesPerSecondLabel.stringValue = [NSString stringWithFormat:@"%1.1ld", (long)self.framesPerSecondSlider.integerValue];
}

- (IBAction)generateImages:(id)sender {
	NSSavePanel *panel = [NSSavePanel savePanel];

	[panel beginSheetModalForWindow:self.view.window completionHandler:^(NSInteger result){
		if (result == NSFileHandlingPanelOKButton)
		{
			NSInteger segment1 = self.segment1Slider.integerValue;
			NSInteger segment2 = self.segment2Slider.integerValue;
			NSInteger totalDuration = segment1 + segment2;
			NSInteger fps = self.framesPerSecondSlider.integerValue;
			
			NSURL *baseFileName = [panel URL];
			NSURL *parentPath = [baseFileName URLByDeletingLastPathComponent];
			NSString *filenameWithExtension = [baseFileName lastPathComponent];
			NSString *filename = [filenameWithExtension stringByDeletingPathExtension];
			NSString *extension = filenameWithExtension.pathExtension;
			
			NSInteger imageNumber = 0;
			for (int i = 0; i < totalDuration; i++) {
				for (int j = 0; j < fps; j++) {
					CGFloat currentDuration = i + ((CGFloat)j / fps);
					NSImage *imageFrame = [CountdownStyleKit imageOfSegmentedTimerWithTotalProgress:currentDuration segment1Duration:segment1 segment2Duration:segment2];
					
					NSString *frameFileName = [NSString stringWithFormat:@"%@-%li@2x.%@", filename, imageNumber, extension];
					NSURL *numberedFilePath = [NSURL URLWithString:frameFileName relativeToURL:parentPath];
					[self saveImage:imageFrame atPath:numberedFilePath];
					imageNumber++;
				}
			}
		}
	}];
}

#pragma mark - Private

- (void)updatePreview
{
	NSInteger totalDuration = self.segment1Slider.integerValue + self.segment2Slider.integerValue;
	[self.previewImage setImage:[CountdownStyleKit imageOfSegmentedTimerWithTotalProgress:totalDuration segment1Duration:self.segment1Slider.integerValue segment2Duration:self.segment2Slider.integerValue]];
}

- (void)saveImage:(NSImage *)image atPath:(NSURL *)path
{
	CGImageRef cgRef = [image CGImageForProposedRect:NULL context:nil hints:nil];
	NSBitmapImageRep *newRep = [[NSBitmapImageRep alloc] initWithCGImage:cgRef];
	[newRep setSize:[image size]];   // if you want the same resolution
	NSData *pngData = [newRep representationUsingType:NSPNGFileType properties:nil];
	[pngData writeToURL:path atomically:YES];
}

@end
