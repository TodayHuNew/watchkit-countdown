//
//  main.m
//  Animation Generation
//
//  Created by Daniel Larsen on 1/28/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
