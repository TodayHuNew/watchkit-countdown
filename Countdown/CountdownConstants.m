//
//  CountdownConstants.m
//  Countdown
//
//  Created by Daniel Larsen on 12/31/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import "CountdownConstants.h"

/// WatchKit extension request keys
NSString * const CountdownExtensionRequestActionKey = @"CountdownExtensionRequestActionKey";
NSString * const CountdownExtensionRequestLatestSegmentAction = @"CountdownExtensionRequestLatestSegmentAction";

/// WatchKit extension request reply
NSString * const CountdownExtensionRequestLatestSegment1Value = @"CountdownExtensionRequestLatestSegment1Value";
NSString * const CountdownExtensionRequestLatestSegment2Value = @"CountdownExtensionRequestLatestSegment2Value";
NSString * const CountdownExtensionRequestLatestAnimationFramesPerSecond = @"CountdownExtensionRequestLatestAnimationFramesPerSecond";

/// User defaults
NSString * const CountdownExtensionSegment1ValueUserDefault = @"CountdownExtensionSegment1ValueUserDefault";
NSString * const CountdownExtensionSegment2ValueUserDefault = @"CountdownExtensionSegment2ValueUserDefault";
NSString * const CountdownExtensionFramesPerSecondUserDefault = @"CountdownExtensionFramesPerSecondUserDefault";

/// Image names
NSString * const CountdownAnimateFullDurationImageName = @"FullCountdown";
NSString * const CountdownAnimatePartialDurationImageName = @"PartialCountdown";
NSString * const CountdownAnimatePreGeneratedAssetImageName = @"Timer-";

/// Image properties
NSInteger const CountdownAnimatePreGeneratedTotalDuration = 10;
NSInteger const CountdownAnimatePreGeneratedTotalImageCount = 300;
