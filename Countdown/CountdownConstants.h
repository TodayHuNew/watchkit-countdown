//
//  CountdownConstants.h
//  Countdown
//
//  Created by Daniel Larsen on 12/31/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

/// WatchKit extension request keys
extern NSString * const CountdownExtensionRequestActionKey;
extern NSString * const CountdownExtensionRequestLatestSegmentAction;

/// WatchKit extension request reply
extern NSString * const CountdownExtensionRequestLatestSegment1Value;
extern NSString * const CountdownExtensionRequestLatestSegment2Value;
extern NSString * const CountdownExtensionRequestLatestAnimationFramesPerSecond;

/// User defaults
extern NSString * const CountdownExtensionSegment1ValueUserDefault;
extern NSString * const CountdownExtensionSegment2ValueUserDefault;
extern NSString * const CountdownExtensionFramesPerSecondUserDefault;

/// Image names
extern NSString * const CountdownAnimateFullDurationImageName;
extern NSString * const CountdownAnimatePartialDurationImageName;
extern NSString * const CountdownAnimatePreGeneratedAssetImageName;

/// Image properties
extern NSInteger const CountdownAnimatePreGeneratedTotalDuration;
extern NSInteger const CountdownAnimatePreGeneratedTotalImageCount;
