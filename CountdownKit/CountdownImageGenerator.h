//
//  CountdownImageGenerator.h
//  Countdown
//
//  Created by Daniel Larsen on 12/31/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UIKit;

@interface CountdownImageGenerator : NSObject

+ (NSArray *)imagesForSegments:(NSArray	*)segmentValues withTotalDuration:(NSInteger)duration atFramesPerSecond:(NSInteger)fps;

+ (NSArray *)imagesForSegments:(NSArray *)segmentValues startingAtTime:(NSTimeInterval)start withTotalDuration:(NSInteger)duration atFramesPerSecond:(NSInteger)fps;

+ (UIImage *)imageForSegments:(NSArray *)segmentValues withRemainingDuration:(CGFloat)remaining;

+ (void)updateImageHeight:(NSInteger)height;

@end
