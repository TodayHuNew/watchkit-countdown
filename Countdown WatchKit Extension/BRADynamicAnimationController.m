//
//  InterfaceController.m
//  Countdown WatchKit Extension
//
//  Created by Daniel Larsen on 12/29/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import <malloc/malloc.h>
#import "BRADynamicAnimationController.h"
#import "CountdownImageGenerator.h"
#import "CountdownConstants.h"

@interface BRADynamicAnimationController()

@property (weak, nonatomic) IBOutlet WKInterfaceImage *progressImage;
@property (weak, nonatomic) IBOutlet WKInterfaceButton *startResumeButton;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *stoppedResetGroup;

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, assign) NSTimeInterval remainingTime;
@property (nonatomic, assign) NSTimeInterval totalDuration;
@property (nonatomic, assign) BOOL isCounting;
@property (nonatomic, assign) NSUInteger imagesCount;
@property (nonatomic, assign) NSUInteger framesPerSecond;
@property (nonatomic, strong) NSArray *segmentValues;

@end


@implementation BRADynamicAnimationController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
	
	CGRect deviceScreen = [[WKInterfaceDevice currentDevice] screenBounds];
	if(CGRectGetHeight(deviceScreen) == 195)
	{
		[CountdownImageGenerator updateImageHeight:123];
		[self.progressImage setHeight:123];
	}
	
	[self pushLatestAnimation];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

- (IBAction)startPauseCountdown {
	if (self.isCounting) {
		[self.progressImage stopAnimating];
		[self.progressImage setImage:nil];
		self.remainingTime += [self.startTime timeIntervalSinceNow];
		if (self.remainingTime < 0) {
			self.remainingTime = 0;
		}
		self.isCounting = NO;
		[self.stoppedResetGroup setHidden:NO];
		[self.startResumeButton setHidden:YES];
		
		__weak typeof(self) weakSelf = self;
		dispatch_async(dispatch_get_main_queue(), ^() {
			
			[weakSelf.progressImage setImage:[CountdownImageGenerator imageForSegments:self.segmentValues withRemainingDuration:self.remainingTime]];
		});
	} else {
		if (self.remainingTime == 0) {
			self.remainingTime = self.totalDuration;
		}
		
		NSInteger startRange = (self.totalDuration - self.remainingTime) * self.framesPerSecond;
		[self.progressImage setImageNamed:CountdownAnimateFullDurationImageName];
		[self.progressImage startAnimatingWithImagesInRange:NSMakeRange(startRange, self.imagesCount) duration:self.remainingTime repeatCount:1];
		
		self.startTime = [NSDate date];
		self.isCounting = YES;
		[self.startResumeButton setTitle:@"Stop"];
		[self.stoppedResetGroup setHidden:YES];
		[self.startResumeButton setHidden:NO];
	}
}

#pragma mark - Actions

- (IBAction)resetCountdown {
	self.startTime = nil;
	self.remainingTime = 0;
	[self.progressImage stopAnimating];
	[self.progressImage setImageNamed:CountdownAnimateFullDurationImageName];
	[self.stoppedResetGroup setHidden:NO];
	[self.startResumeButton setHidden:YES];
}

- (IBAction)generateNewAnimation {
	[self.progressImage setImage:nil];
	[self pushLatestAnimation];
}

#pragma mark - Private

- (void)pushLatestAnimation {
	__weak typeof(self) weakSelf = self;
	[WKInterfaceController openParentApplication:@{ CountdownExtensionRequestActionKey : CountdownExtensionRequestLatestSegmentAction } reply:^(NSDictionary *reply, NSError *error) {
		
		NSNumber *segment1Value = [reply valueForKey:CountdownExtensionRequestLatestSegment1Value];
		NSNumber *segment2Value = [reply valueForKey:CountdownExtensionRequestLatestSegment2Value];
		NSNumber *animationFramesPerSecond = [reply valueForKey:CountdownExtensionRequestLatestAnimationFramesPerSecond];
		
		weakSelf.framesPerSecond = [animationFramesPerSecond integerValue];
		weakSelf.segmentValues = @[ segment1Value, segment2Value ];
		weakSelf.totalDuration = [segment1Value integerValue] + [segment2Value integerValue];
		NSArray *allImages = [CountdownImageGenerator imagesForSegments:weakSelf.segmentValues withTotalDuration:weakSelf.totalDuration atFramesPerSecond:weakSelf.framesPerSecond];
		[weakSelf logAnimationImageSizes:allImages];
		UIImage *countAll = [UIImage animatedImageWithImages:allImages duration:weakSelf.totalDuration];
		weakSelf.imagesCount = [[countAll images] count];
		[[WKInterfaceDevice currentDevice] removeCachedImageWithName:CountdownAnimateFullDurationImageName];
		BOOL imageAdded = [[WKInterfaceDevice currentDevice] addCachedImage:countAll name:CountdownAnimateFullDurationImageName];
		NSAssert(imageAdded, @"Unable to add image to cache");
		
		[weakSelf.progressImage setImageNamed:CountdownAnimateFullDurationImageName];
	}];
}

- (void)logAnimationImageSizes:(NSArray *)animationImages
{
	int totalSize = 0;
	for (id img in animationImages) {
		totalSize += malloc_size((__bridge const void *) img);
	}
	NSLog(@"%@", [NSByteCountFormatter stringFromByteCount:totalSize countStyle:NSByteCountFormatterCountStyleMemory]);
}


@end



