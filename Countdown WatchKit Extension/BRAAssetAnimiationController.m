//
//  BRAAssetAnimiationControllerInterfaceController.m
//  Countdown
//
//  Created by Daniel Larsen on 1/27/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "BRAAssetAnimiationController.h"
#import "CountdownConstants.h"

@interface BRAAssetAnimiationController()

@property (nonatomic, assign) BOOL isCounting;
@property (nonatomic, assign) NSTimeInterval remainingTime;
@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, assign) NSUInteger framesPerSecond;

@property (weak, nonatomic) IBOutlet WKInterfaceImage *progressImage;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *stoppedResetGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceButton *startResumeButton;

@end


@implementation BRAAssetAnimiationController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
	
	self.framesPerSecond = CountdownAnimatePreGeneratedTotalImageCount / CountdownAnimatePreGeneratedTotalDuration;
	[self setProgressNumber:0];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

#pragma mark - Actions

- (IBAction)startPauseCountdown {
	if (self.isCounting) {
		[self.progressImage stopAnimating];
		self.remainingTime += [self.startTime timeIntervalSinceNow];
		if (self.remainingTime < 0) {
			self.remainingTime = 0;
		}
		self.isCounting = NO;
		[self.stoppedResetGroup setHidden:NO];
		[self.startResumeButton setHidden:YES];
		[self setProgressNumber:((CountdownAnimatePreGeneratedTotalDuration - self.remainingTime) * self.framesPerSecond)];
	} else {
		if (self.remainingTime == 0) {
			self.remainingTime = CountdownAnimatePreGeneratedTotalDuration;
		}
		
		NSInteger startRange = (CountdownAnimatePreGeneratedTotalDuration - self.remainingTime) * self.framesPerSecond;
		[self.progressImage setImageNamed:CountdownAnimatePreGeneratedAssetImageName];
		[self.progressImage startAnimatingWithImagesInRange:NSMakeRange(startRange, CountdownAnimatePreGeneratedTotalImageCount) duration:self.remainingTime repeatCount:1];
		
		self.startTime = [NSDate date];
		self.isCounting = YES;
		[self.startResumeButton setTitle:@"Stop"];
		[self.stoppedResetGroup setHidden:YES];
		[self.startResumeButton setHidden:NO];
	}
}

- (IBAction)resetCountdown
{
	self.startTime = nil;
	self.remainingTime = 0;
	[self.progressImage stopAnimating];
	[self setProgressNumber:0];
	[self.stoppedResetGroup setHidden:NO];
	[self.startResumeButton setHidden:YES];
}

#pragma mark - Private

- (void)setProgressNumber:(NSInteger)frameNumber
{
	NSString *imageName = [NSString stringWithFormat:@"%@%li", CountdownAnimatePreGeneratedAssetImageName, frameNumber];
	[self.progressImage setImageNamed:imageName];
}

@end



